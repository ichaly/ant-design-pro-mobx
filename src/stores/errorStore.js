import { observable, action, runInAction } from 'mobx'
import { query as queryError } from '../services/error'

class ErrorStore {
  @observable error = null

  @observable loading = false

  @action
  async query (code) {
    this.loading = true
    const data = await queryError(code)
    runInAction(() => {
      this.error = code
      this.loading = false
    })
  }
}

export default new ErrorStore()
