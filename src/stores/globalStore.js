import { observable, action, runInAction, computed } from 'mobx'
import { queryNotices } from 'services/api'

class GlobalStore {
  @observable collapsed = false

  @observable notices = []

  @computed
  get noticeCount () {
    return this.notices.length
  }

  @action
  async fetchNotices () {
    const data = await queryNotices()
    runInAction(() => {
      this.notices = data
    })
  }

  @action
  saveNotices (notices) {
    this.notices = notices
  }

  @action
  saveClearedNotices (type) {
    this.notices = this.notices.filter(item => item.type !== type)
  }

  @action
  changeLayoutCollapsed (collapsed) {
    this.collapsed = collapsed
  }
}

export default new GlobalStore()
