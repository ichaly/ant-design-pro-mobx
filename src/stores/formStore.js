import { observable, action, runInAction } from 'mobx'
import { message } from 'antd'
import { fakeSubmitForm } from '../services/api'

class FormStore {
  @observable step = {
    payAccount: 'ant-design@alipay.com',
    receiverAccount: 'test@example.com',
    receiverName: 'Alex',
    amount: '500',
  }

  @observable loading = true

  @action
  async submitRegularForm () {
    await fakeSubmitForm()
    message.success('提交成功')
  }

  @action
  async submitStepForm (payload) {
    await fakeSubmitForm(payload)
    runInAction(() => {
      this.saveStepFormData(payload)
    })
  }

  @action
  saveStepFormData (payload) {
    this.step = {
      ...this.step,
      ...payload,
    }
  }

  @action
  async submitAdvancedForm (payload) {
    await fakeSubmitForm(payload)
    message.success('提交成功')
  }
}

export default new FormStore()
