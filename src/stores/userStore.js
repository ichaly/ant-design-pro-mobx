import { observable, action, runInAction } from 'mobx'
import {
  query as queryUsers,
  queryCurrent,
} from '../services/user'

class UserStore {
  @observable list = []

  @observable currentUser = {}

  @action
  async fetch () {
    const data = await queryUsers()

    runInAction(() => {
      this.list = data
    })
  }
  async fetchCurrent () {
    const data = await queryCurrent()

    runInAction(() => {
      this.currentUser = data
    })
  }

  @action
  changeLayoutCollapsed (collapsed) {
    this.collapsed = collapsed
  }
}

export default new UserStore()
