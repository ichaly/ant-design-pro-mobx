import { observable, action, runInAction } from 'mobx'
import { queryFakeList } from '../services/api'

class ListStore {
  @observable list = []

  @observable loading = true

  @action
  async fetch (payload) {
    const data = await queryFakeList(payload)
    runInAction(() => {
      this.list = Array.isArray(data) ? data : []

      this.loading = false
    })
  }

  @action
  async appendFetch (payload) {
    const data = await queryFakeList(payload)
    runInAction(() => {
      this.list = this.list.concat(Array.isArray(data) ? data : [])
    })
  }
}

export default new ListStore()
