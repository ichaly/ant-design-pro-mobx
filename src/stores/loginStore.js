import { observable, action, runInAction } from 'mobx'
import { fakeAccountLogin } from '../services/api'
import { setAuthority } from '../utils/authority'
import { reloadAuthorized } from '../utils/Authorized'

class LoginStore {
  @observable status = undefined
  @observable submitting = false

  @action
  async login (payload) {
    this.submitting = true
    const data = await fakeAccountLogin(payload)

    setAuthority(data.currentAuthority)
    reloadAuthorized()
    runInAction(() => {
      this.status = data.status
      this.submitting = false
    })

    return data.status
  }

  @action
  logout () {
    try {
      // get location pathname
      const urlParams = new URL(window.location.href)
      // const pathname = yield select(state => state.routing.location.pathname);
      // // add the parameters in the url
      // urlParams.searchParams.set('redirect', pathname);
      window.history.replaceState(null, 'login', urlParams.href)
    } finally {
      setAuthority('guest')
      this.status = false
      reloadAuthorized()
    }
  }
}

export default new LoginStore()
