import { observable, action, runInAction } from 'mobx'
import { fakeChartData } from '../services/api'

class ChartStore {
  @observable chart = {
    visitData: [],
    visitData2: [],
    salesData: [],
    searchData: [],
    offlineData: [],
    offlineChartData: [],
    salesTypeData: [],
    salesTypeDataOnline: [],
    salesTypeDataOffline: [],
    radarData: [],
  }

  @observable loading = true

  @action
  async fetch () {
    const data = await fakeChartData()
    runInAction(() => {
      this.chart = {
        ...data,
      }

      this.loading = false
    })
  }

  @action
  async fetchSalesData () {
    const data = await fakeChartData()
    runInAction(() => {
      this.chart.salesData = data.salesData
    })
  }

  @action
  clear () {
    this.chart = {
      visitData: [],
      visitData2: [],
      salesData: [],
      searchData: [],
      offlineData: [],
      offlineChartData: [],
      salesTypeData: [],
      salesTypeDataOnline: [],
      salesTypeDataOffline: [],
      radarData: [],
    }
  }
}

export default new ChartStore()
