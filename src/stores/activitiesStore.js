import { observable, action, runInAction } from 'mobx'
import { queryActivities } from '../services/api'

class ActivitiesStore {
  @observable list = []

  @observable loading = true

  @action
  async fetchList () {
    const data = await queryActivities()
    runInAction(() => {
      this.list = Array.isArray(data) ? data : []
      this.loading = false
    })
  }
}

export default new ActivitiesStore()
