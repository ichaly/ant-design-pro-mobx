import { observable, action, runInAction } from 'mobx'
import { queryRule, removeRule, addRule } from '../services/api'

class RuleStore {
  @observable data = {
    list: [],
    pagination: {},
  }

  @observable loading = true

  @action
  async fetch (payload) {
    const data = await queryRule(payload)
    runInAction(() => {
      this.data = data

      this.loading = false
    })
  }

  @action
  async add (payload, callback) {
    const data = await addRule(payload)
    runInAction(() => {
      this.data = data

      if (callback) callback()
    })
  }

  @action
  async remove (payload, callback) {
    const data = await removeRule(payload)
    runInAction(() => {
      this.data = data

      if (callback) callback()
    })
  }
}

export default new RuleStore()
