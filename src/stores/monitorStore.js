import { observable, action, runInAction } from 'mobx'
import { queryTags } from '../services/api'

class MonitorStore {
  @observable tags = []

  @observable loading = true

  @action
  async fetchTags () {
    const data = await queryTags()
    runInAction(() => {
      this.tags = data.list
      this.loading = false
    })
  }
}

export default new MonitorStore()
