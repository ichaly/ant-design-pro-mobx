import { observable, action, runInAction } from 'mobx'
import { fakeRegister } from '../services/api'
import { setAuthority } from '../utils/authority'
import { reloadAuthorized } from '../utils/Authorized'

class RegisterStore {
  @observable status = undefined

  @observable loading = true

  @action
  async submit () {
    const data = await fakeRegister()

    setAuthority('user')

    reloadAuthorized()
    runInAction(() => {
      this.status = data.status

      this.loading = false
    })
  }
}

export default new RegisterStore()
