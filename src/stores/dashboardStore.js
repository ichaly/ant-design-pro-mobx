import { observable, action, runInAction } from 'mobx'
import axios from 'axios'

// mobx.useStrict(true)

class DashboardStore {
  @observable chart = {
    visitData: [],
    visitData2: [],
    salesData: [],
    searchData: [],
    offlineData: [],
    offlineChartData: [],
    salesTypeData: [],
    salesTypeDataOnline: [],
    salesTypeDataOffline: [],
  }

  @observable loadings = {
    analysis: true,
  }

  @action
  async loadChart () {
    const { data: chart } = await axios.get('https://www.easy-mock.com/mock/59bb875fe0dc663341ab8e8f/yk/mobx')
    runInAction(() => {
      this.chart = chart
      this.loadings.analysis = false
    })
  }
}

export default new DashboardStore()
