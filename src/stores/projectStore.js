import { observable, action, runInAction } from 'mobx'
import { queryProjectNotice } from '../services/api';

class ProjectStore {
  @observable notice = []
  @observable loading = true

  @action
  async fetchNotice () {
    const data = await queryProjectNotice()
    runInAction(() => {
      this.notice = Array.isArray(data) ? data : []
      this.loading = false
    })
  }
}

export default new ProjectStore()
