import { observable, action, runInAction } from 'mobx'
import { queryBasicProfile, queryAdvancedProfile } from '../services/api'

class formStore {
  @observable profile = {
    basicGoods: [],
    advancedOperation1: [],
    advancedOperation2: [],
    advancedOperation3: [],
  }

  @observable loading = true

  @action
  async fetchBasic () {
    const data = await queryBasicProfile()

    runInAction(() => {
      this.profile = { ...data }
      this.loading = false
    })
  }

  @action
  async fetchAdvanced () {
    const data = await queryAdvancedProfile()

    runInAction(() => {
      this.profile = { ...data }
      this.loading = false
    })
  }
}

export default new formStore()
