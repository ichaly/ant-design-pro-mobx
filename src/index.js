import React from 'react'
import ReactDOM from 'react-dom'
import { HashRouter, Route, Switch } from 'react-router-dom'
import { Provider } from 'mobx-react'
import { LocaleProvider } from 'antd'

import createHistory from 'history/createHashHistory'
import zhCN from 'antd/lib/locale-provider/zh_CN'
import 'moment/locale/zh-cn'

import Authorized from './utils/Authorized'
import routerData from './router'
import './rollbar'
import './index.less'

// import UserLayout from 'layouts/UserLayout'
// import BasicLayout from 'layouts/BasicLayout'

import globalStore from 'stores/globalStore'
import userStore from 'stores/userStore'
import chartStore from 'stores/chartStore'
import monitorStore from 'stores/monitorStore'
import projectStore from 'stores/projectStore'
import activitiesStore from 'stores/activitiesStore'
import formStore from 'stores/formStore'
import listStore from 'stores/listStore'
import ruleStore from 'stores/ruleStore'
import profileStore from 'stores/profileStore'
import loginStore from 'stores/loginStore'
import registerStore from 'stores/registerStore'
import errorStore from 'stores/errorStore'

// import AuthorizedRoute from 'components/Authorized/AuthorizedRoute'
const { AuthorizedRoute } = Authorized

const stores = {
  globalStore,
  userStore,
  chartStore,
  monitorStore,
  projectStore,
  activitiesStore,
  formStore,
  listStore,
  ruleStore,
  profileStore,
  loginStore,
  registerStore,
  errorStore,
}

const UserLayout = routerData['/user'].component
const BasicLayout = routerData['/'].component

ReactDOM.render(
  <LocaleProvider locale={zhCN}>
    <Provider {...stores}>
      <HashRouter>
        <Switch>
          <Route path='/user' component={UserLayout} />
          <AuthorizedRoute
            path='/'
            render={props => <BasicLayout routerData={routerData} {...props} />}
            authority={['admin', 'user']}
            redirectPath='/user/login'
          />
        </Switch>
      </HashRouter>
    </Provider>
  </LocaleProvider>,
  document.getElementById('root')
)
