import React from 'react'
import Loadable from 'react-loadable'
import pathToRegexp from 'path-to-regexp'
import { Spin } from 'antd'
import styles from './index.less'
import { getMenuData } from './common/menu'

function antLoadable (component) {
  return Loadable({
    loader: component,
    loading: () => (<Spin size='large' className={styles.globalSpin} />),
    delay: 0,
  })
}

function getFlatMenuData (menus) {
  let keys = {}
  menus.forEach(item => {
    if (item.children) {
      keys[item.path] = { ...item }
      keys = { ...keys, ...getFlatMenuData(item.children) }
    } else {
      keys[item.path] = { ...item }
    }
  })
  return keys
}

const routerConfig = {
  '/': {
    component: antLoadable(() => import('./layouts/BasicLayout')),
  },
  '/dashboard/analysis': {
    component: antLoadable(() => import('./routes/Dashboard/Analysis')),
  },
  '/dashboard/monitor': {
    component: antLoadable(() => import('./routes/Dashboard/Monitor')),
  },
  '/dashboard/workplace': {
    component: antLoadable(() => import('./routes/Dashboard/Workplace')),
    // hideInBreadcrumb: true,
    // name: '工作台',
    // authority: 'admin',
  },
  '/form/basic-form': {
    component: antLoadable(() => import('./routes/Forms/BasicForm')),
  },
  '/form/step-form': {
    component: antLoadable(() => import('./routes/Forms/StepForm')),
  },
  '/form/step-form/info': {
    name: '分步表单（填写转账信息）',
    component: antLoadable(() => import('./routes/Forms/StepForm/Step1')),
  },
  '/form/step-form/confirm': {
    name: '分步表单（确认转账信息）',
    component: antLoadable(() => import('./routes/Forms/StepForm/Step2')),
  },
  '/form/step-form/result': {
    name: '分步表单（完成）',
    component: antLoadable(() => import('./routes/Forms/StepForm/Step3')),
  },
  '/form/advanced-form': {
    component: antLoadable(() => import('./routes/Forms/AdvancedForm')),
  },
  '/list/table-list': {
    component: antLoadable(() => import('./routes/List/TableList')),
  },
  '/list/basic-list': {
    component: antLoadable(() => import('./routes/List/BasicList')),
  },
  '/list/card-list': {
    component: antLoadable(() => import('./routes/List/CardList')),
  },
  '/list/search': {
    component: antLoadable(() => import('./routes/List/List')),
  },
  '/list/search/projects': {
    component: antLoadable(() => import('./routes/List/Projects')),
  },
  '/list/search/applications': {
    component: antLoadable(() => import('./routes/List/Applications')),
  },
  '/list/search/articles': {
    component: antLoadable(() => import('./routes/List/Articles')),
  },
  '/profile/basic': {
    component: antLoadable(() => import('./routes/Profile/BasicProfile')),
  },
  '/profile/advanced': {
    component: antLoadable(() => import('./routes/Profile/AdvancedProfile')),
  },
  '/result/success': {
    component: antLoadable(() => import('./routes/Result/Success')),
  },
  '/result/fail': {
    component: antLoadable(() => import('./routes/Result/Error')),
  },
  '/exception/403': {
    component: antLoadable(() => import('./routes/Exception/403')),
  },
  '/exception/404': {
    component: antLoadable(() => import('./routes/Exception/404')),
  },
  '/exception/500': {
    component: antLoadable(() => import('./routes/Exception/500')),
  },
  '/exception/trigger': {
    component: antLoadable(() => import('./routes/Exception/triggerException')),
  },
  '/user': {
    component: antLoadable(() => import('./layouts/UserLayout')),
  },
  '/user/login': {
    component: antLoadable(() => import('./routes/User/Login')),
  },
  '/user/register': {
    component: antLoadable(() => import('./routes/User/Register')),
  },
  '/user/register-result': {
    component: antLoadable(() => import('./routes/User/RegisterResult')),
  },
}

const menuData = getFlatMenuData(getMenuData())
// Route configuration data
// eg. {name,authority ...routerConfig }
const routerData = {}
// The route matches the menu
Object.keys(routerConfig).forEach(path => {
  // Regular match item name
  // eg.  router /user/:id === /user/chen
  const pathRegexp = pathToRegexp(path)
  const menuKey = Object.keys(menuData).find(key => pathRegexp.test(`${key}`))
  let menuItem = {}
  // If menuKey is not empty
  if (menuKey) {
    menuItem = menuData[menuKey]
  }
  let router = routerConfig[path]
  // If you need to configure complex parameter routing,
  // https://github.com/ant-design/ant-design-pro-site/blob/master/docs/router-and-nav.md#%E5%B8%A6%E5%8F%82%E6%95%B0%E7%9A%84%E8%B7%AF%E7%94%B1%E8%8F%9C%E5%8D%95
  // eg . /list/:type/user/info/:id
  router = {
    ...router,
    name: router.name || menuItem.name,
    authority: router.authority || menuItem.authority,
    hideInBreadcrumb: router.hideInBreadcrumb || menuItem.hideInBreadcrumb,
  }
  routerData[path] = router
})
export default routerData
