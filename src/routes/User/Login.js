import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'
import { Checkbox, Alert, Icon } from 'antd'
import Login from 'components/Login'
import styles from './Login.less'

const { Tab, UserName, Password, Mobile, Captcha, Submit } = Login

@inject('loginStore')
@withRouter
@observer
export default class LoginPage extends Component {
  state = {
    type: 'account',
    autoLogin: true,
  };

  onTabChange = type => {
    this.setState({ type })
  };

  handleSubmit = async (err, values) => {
    const { type } = this.state
    const { history } = this.props
    if (!err) {
      const status = await this.props.loginStore.login({ ...values, type })
      if (status !== 'error') history.push('/')
    }
  };

  changeAutoLogin = e => {
    this.setState({
      autoLogin: e.target.checked,
    })
  };

  renderMessage = content => {
    return <Alert style={{ marginBottom: 24 }} message={content} type='error' showIcon />
  };

  render () {
    const { status, login, submitting } = this.props.loginStore
    const { type } = this.state
    return (
      <div className={styles.main}>
        <Login defaultActiveKey={type} onTabChange={this.onTabChange} onSubmit={this.handleSubmit}>
          <Tab key='account' tab='账户密码登录'>
            {status === 'error' &&
              type === 'account' &&
              !submitting &&
              this.renderMessage('账户或密码错误（admin/888888）')}
            <UserName name='userName' placeholder='admin/user' />
            <Password name='password' placeholder='888888/123456' />
          </Tab>
          <Tab key='mobile' tab='手机号登录'>
            {status === 'error' &&
              type === 'mobile' &&
              !submitting &&
              this.renderMessage('验证码错误')}
            <Mobile name='mobile' />
            <Captcha name='captcha' />
          </Tab>
          <div>
            <Checkbox checked={this.state.autoLogin} onChange={this.changeAutoLogin}>
              自动登录
            </Checkbox>
            <a style={{ float: 'right' }} href=''>
              忘记密码
            </a>
          </div>
          <Submit loading={submitting}>登录</Submit>
          <div className={styles.other}>
            其他登录方式
            <Icon className={styles.icon} type='alipay-circle' />
            <Icon className={styles.icon} type='taobao-circle' />
            <Icon className={styles.icon} type='weibo-circle' />
            <Link className={styles.register} to='/user/register'>
              注册账户
            </Link>
          </div>
        </Login>
      </div>
    )
  }
}
