import React, { Fragment } from 'react'
import { inject, observer } from 'mobx-react'
import { withRouter } from 'react-router-dom'
import { Button, Row, Col } from 'antd'
import Result from 'components/Result'
import styles from './style.less'

@inject('formStore')
@withRouter
@observer
export default class Step3 extends React.Component {
  render () {
    const { history, formStore: { step } } = this.props
    const onFinish = () => {
      history.push('/form/step-form')
    }
    const information = (
      <div className={styles.information}>
        <Row>
          <Col span={8} className={styles.label}>
            付款账户：
          </Col>
          <Col span={16}>{step.payAccount}</Col>
        </Row>
        <Row>
          <Col span={8} className={styles.label}>
            收款账户：
          </Col>
          <Col span={16}>{step.receiverAccount}</Col>
        </Row>
        <Row>
          <Col span={8} className={styles.label}>
            收款人姓名：
          </Col>
          <Col span={16}>{step.receiverName}</Col>
        </Row>
        <Row>
          <Col span={8} className={styles.label}>
            转账金额：
          </Col>
          <Col span={16}>
            <span className={styles.money}>{step.amount}</span> 元
          </Col>
        </Row>
      </div>
    )
    const actions = (
      <Fragment>
        <Button type='primary' onClick={onFinish}>
          再转一笔
        </Button>
        <Button>查看账单</Button>
      </Fragment>
    )
    return (
      <Result
        type='success'
        title='操作成功'
        description='预计两小时内到账'
        extra={information}
        actions={actions}
        className={styles.result}
      />
    )
  }
}
