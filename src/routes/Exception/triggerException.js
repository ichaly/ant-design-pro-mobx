import React, { Component } from 'react'
import { Button, Spin, Card } from 'antd'
import { inject, observer } from 'mobx-react'
import { withRouter } from 'react-router-dom'
import styles from './style.less'

@inject('errorStore')
@observer
@withRouter
export default class TriggerException extends Component {
  triggerError = async (code) => {
    await this.props.errorStore.query(code)
    this.props.history.push(`/exception/${code}`)
  };
  render () {
    return (
      <Card>
        <Spin spinning={this.props.errorStore.loading} wrapperClassName={styles.trigger}>
          <Button type='danger' onClick={() => this.triggerError(401)}>
            触发401
          </Button>
          <Button type='danger' onClick={() => this.triggerError(403)}>
            触发403
          </Button>
          <Button type='danger' onClick={() => this.triggerError(500)}>
            触发500
          </Button>
          <Button type='danger' onClick={() => this.triggerError(404)}>
            触发404
          </Button>
        </Spin>
      </Card>
    )
  }
}
